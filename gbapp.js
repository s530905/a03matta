var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var server = require('http').createServer(app); // inject app into the server

// set up the view engine
// manage our entries
// set up the logger
// GETS
// POSTS
// 404
// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine
app.use(express.static(__dirname + '/assets'))

// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response){
  response.sendFile(__dirname+"/views/matta_chaithanyamanasreddy.html");
});
app.get("/about", function (request, response){
  response.sendFile(__dirname+"/views/matta_chaithanyamanasreddy.html");
});
app.get("/simpleinterest", function (request, response){
  response.sendFile(__dirname+"/views/Simpleinterest.html");
});
app.get("/contact", function (request, response){
  response.sendFile(__dirname+"/views/contact.html");
});





app.get("/guestbook", function (request, response) {
  response.render("index")
})
app.get("/new-entry", function (request, response) {
  response.render("new-entry")
})
// 5 handle an http POST request to the new-entry URI 

app.post("/contact", function (request, response){
  var api_key = 'key-fef56cc42c2eb601fe02be50287f7939';
  var domain = 'sandboxb12eefc99ffa4e8b84dad321259a5307.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'mailgun <postmaster@sandboxb12eefc99ffa4e8b84dad321259a5307.mailgun.org>',
    to: 'chaitu.manas092@gmail.com',
    subject: 'Got mail from '+request.body.LastName+", "+request.body.FirstName,
    text: "Full Name :"+request.body.LastName+", "+request.body.FirstName+"\n"+
    "EmailID: "+request.body.EmailID+"\n\n\n"+
    "You got Some Remarks: "+request.body.Remarks
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(error){
    response.send("mail not sent");
    }
    else{
      response.send("mail sent successfully");
    }
    
  });

});
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  response.redirect("/")  // where to go next? Let's go to the home page :)
})

// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404")
})

// Listen for an application request on port 8081 & notify the developer
//http.listen(8081, function () {
 // console.log('Guestbook app listening on http://127.0.0.1:8081/')
//})




// Listen for an application request on port 8081
server.listen(8085, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8085/');
});







